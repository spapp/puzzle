#!/bin/sh
#
# puzzle - Graduated database building.
#
# GNU General Public License v3 <http://www.gnu.org/licenses/>
# Copyright (c) 2013 Sandor Papp
#
# <https://bitbucket.org/spapp/puzzle>
#

# gets arguments
while [ $# -gt 0 ]
do
    case ${1} in
        '-a' | '--adapter')         shift; DB_ADAPTER=${1};;
        '-s' | '--schema')          shift; SCHEMA_NAME=${1};;
        '-S' | '--schema-path')     shift; SCHEMA_PATH=${1};;
        '-h' | '--host')            shift; DB_HOST=${1};;
        '-u' | '--username')        shift; DB_USER=${1};;
        '-p' | '--password')        shift; DB_PWD=${1};;
        '-d' | '--database')        shift; DB_NAME=${1};;
        '-P' | '--port')            shift; DB_PORT=${1};;
        '-v' | '--update-version')  shift; update_schema_version=${1};;
        '-c' | '--config')          shift; CONFIG_FILE=${1};;
        '-V' | '--version')         echo "${APPLICATION_NAME} ${APPLICATION_VERSION}"; exit 0;;
        '--silent')                 LOG_SILENT=1;;
        '-?' | '--help')            cat ${APPLICATION_MAN};exit 0;;
        *)                          log_warn "Invalid argument ${1}";cat ${APPLICATION_MAN};exit 1;;
    esac

    shift
done

# if the file exists and readable then overwrite arguments
if [ -r "${CONFIG_FILE}" ]
then
    log_info "Include config file: ${CONFIG_FILE}"
    . ${CONFIG_FILE}
elif  [ -r "${APPLICATION_PATH}/${CONFIG_FILE}" ]
then
    log_info "Include config file: ${APPLICATION_PATH}/${CONFIG_FILE}"
    . ${APPLICATION_PATH}/${CONFIG_FILE}
elif [ -n "${CONFIG_FILE}" ]
then
    log_error "Config file is not exists or not readable. (${CONFIG_FILE})"
    exit 1
fi

# checks the adapter is exists
if [ -d "${APPLICATION_PATH}/script/${DB_ADAPTER}" ]
then
    log_info Selected adapter: ${DB_ADAPTER}
else
    log_error The \"${DB_ADAPTER}\" adapter is not exists!
    exit 1
fi

# Checks arguments.
log_info Check arguments.
has_error=0

# include adapter defaults and checks variables
. ${APPLICATION_PATH}/script/${DB_ADAPTER}/defaults.sh

if [ -z "${SCHEMA_NAME}" ]
then
    log_warn The database schema name is required.
    has_error=1
fi

if [ -z "${update_schema_version}" ]
then
    log_warn The database update version is required.
    has_error=1
fi

if [ ${has_error} -gt 0 ]
then
    log_error Some arguments missing.
    exit 1
fi