#!/bin/sh
#
# puzzle - Graduated database building.
#
# GNU General Public License v3 <http://www.gnu.org/licenses/>
# Copyright (c) 2013 Sandor Papp
#
# <https://bitbucket.org/spapp/puzzle>
#
LOG_SILENT=${LOG_SILENT:-0}
LOG_FILE="${APPLICATION_PATH}/log/$(date +"%Y-%m-%d").log"

COLOR_DEFAULT="\033[0;0m"
COLOR_SUCCESS="\033[1;32m"
COLOR_FAIL="\033[1;31m"
COLOR_WARN="\033[1;33m"
COLOR_INFO="\033[1;34m"

LAST_LOG_MESSAGE=

# Log an information message
#
# params:
#   - mixed $1..n messages
#
# globals:
#   - function  log_begin
#   - function  log_to_file
#   - string    $LOG_SILENT
#   - string    $COLOR_INFO
#   - string    $COLOR_DEFAULT
#
# returns
#   - void
#
function log_info () {
    if [ ${LOG_SILENT} -eq 0 ]
    then
        log_begin $@
        echo -e "\r[${COLOR_INFO}INFO${COLOR_DEFAULT}]"
    fi

    log_to_file INFO ${@}
}

# Log a warning message
#
# params:
#   - mixed $1..n messages
#
# globals:
#   - function  log_begin
#   - function  log_to_file
#   - string    $LOG_SILENT
#   - string    $COLOR_WARN
#   - string    $COLOR_DEFAULT
#
# returns
#   - void
#
function log_warn () {
    if [ ${LOG_SILENT} -eq 0 ]
    then
        log_begin $@
        echo -e "\r[${COLOR_WARN}WARN${COLOR_DEFAULT}]"
    fi

    log_to_file WARN ${@}
}

# Log an error message
#
# params:
#   - mixed $1..n messages
#
# globals:
#   - function  log_begin
#   - function  log_to_file
#   - string    $LOG_SILENT
#   - string    $COLOR_FAIL
#   - string    $COLOR_DEFAULT
#
# returns
#   - void
#
function log_error () {
    if [ ${LOG_SILENT} -eq 0 ]
    then
        log_begin $@
        echo -e "\r${COLOR_FAIL}ERROR${COLOR_DEFAULT} "
    fi

    log_to_file ERROR ${@}
}

# Finish with a message 'ok' status.
#
# globals:
#   - function  log_to_file
#   - string    $LOG_SILENT
#   - string    $COLOR_SUCCESS
#   - string    $COLOR_DEFAULT
#   - string    $LAST_LOG_MESSAGE
#
# returns
#   - void
#
function log_end_ok () {
    if [ ${LOG_SILENT} -eq 0 ]
    then
        echo -e "\r[${COLOR_SUCCESS} OK ${COLOR_DEFAULT}]"
    fi

    log_to_file OK ${LAST_LOG_MESSAGE}
}

# Finish with a message 'fail' status.
#
# globals:
#   - function  log_to_file
#   - string    $LOG_SILENT
#   - string    $COLOR_FAIL
#   - string    $COLOR_DEFAULT
#   - string    $LAST_LOG_MESSAGE
#
# returns
#   - void
#
function log_end_fail () {
    if [ ${LOG_SILENT} -eq 0 ]
    then
        echo -e "\r[${COLOR_FAIL}FAIL${COLOR_DEFAULT}]"
    fi

    log_to_file FAIL ${LAST_LOG_MESSAGE}
}

# Begining a meessage.
#
# params:
#   - mixed $1..n messages
#
# globals:
#   - string $LOG_SILENT
#   - string $LAST_LOG_MESSAGE
#
# returns
#   - void
#
function log_begin () {
    if [ ${LOG_SILENT} -eq 0 ]
    then
        echo -ne "[    ] $@"
    fi

    LAST_LOG_MESSAGE="$@"
}

# log a message to the file
# Begining a meessage.
#
# params:
#   - string    $1 log level
#   - mixed     $2..n messages
#
# globals:
#   - string $LOG_FILE
#
# returns
#   - void
#
function log_to_file (){
    level=${1}
    shift

    echo "$(date +"%F %T%:z") [${level}] [$(whoami)] $@" >> ${LOG_FILE}
}
