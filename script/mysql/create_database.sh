#!/bin/sh
#
# puzzle - Graduated database building.
#
# GNU General Public License v3 <http://www.gnu.org/licenses/>
# Copyright (c) 2013 Sandor Papp
#
# <https://bitbucket.org/spapp/puzzle>
#

# Create a database.
#
# params:
#   - string $1 database name
#
# globals:
#   - string $MYSQL_CMD
#   - string $DB_DEFAULT_CHARACTER_SET
#
# returns
#   - 0 if ok
#   - 1 if fail
#
function create_database (){
    sql="CREATE DATABASE ${1} DEFAULT CHARACTER SET ${DB_DEFAULT_CHARACTER_SET};"

    ${MYSQL_CMD} -e "${sql}" > /dev/null 2>&1

    return $?
}