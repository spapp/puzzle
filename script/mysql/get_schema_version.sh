#!/bin/sh
#
# puzzle - Graduated database building.
#
# GNU General Public License v3 <http://www.gnu.org/licenses/>
# Copyright (c) 2013 Sandor Papp
#
# <https://bitbucket.org/spapp/puzzle>
#

# Gets current database schema version.
#
# globals:
#   - string $MYSQL_CMD
#   - string $DB_NAME
#
# returns
#   - 0 if ok
#   - 1 if fail
#
function get_schema_version (){
    tmp=$(mktemp)
    sql='SELECT * FROM database_configuration;'
    csv="$(${MYSQL_CMD} -D ${DB_NAME} -N -e "${sql}" 2>${tmp})"

    tmp_csv=$(echo ${csv} | grep -E ^[0-9]+$)

    if [ "${csv}" == "${tmp_csv}" ] && [ -n "${csv}" ]
    then
        echo ${csv}
        fail=0
    else
        cat ${tmp}
        fail=1
    fi

    rm -f ${tmp}

    return ${fail}
}