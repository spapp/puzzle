#!/bin/sh
#
# puzzle - Graduated database building.
#
# GNU General Public License v3 <http://www.gnu.org/licenses/>
# Copyright (c) 2013 Sandor Papp
#
# <https://bitbucket.org/spapp/puzzle>
#

# Make a database dump.
#
# params:
#   - string $1 database dump folder
#
# globals:
#   - string $DB_HOST
#   - string $DB_USER
#   - string $DB_PWD
#   - string $DB_NAME
#
# returns
#   - 0 if ok
#   - 1 if fail
#
function make_dump (){
    if [ -f "${1}/${DB_NAME}.dump" ]
    then
        cp -fr "${1}/${DB_NAME}.dump" "${1}/${DB_NAME}.old.dump.sql"
    fi

    mysqldump -h ${DB_HOST} -u ${DB_USER} -p${DB_PWD} -B ${DB_NAME} > "${1}/${DB_NAME}.dump.sql"

    return $?
}