#!/bin/sh
#
# puzzle - Graduated database building.
#
# GNU General Public License v3 <http://www.gnu.org/licenses/>
# Copyright (c) 2013 Sandor Papp
#
# <https://bitbucket.org/spapp/puzzle>
#

# Updates database schema version.
#
# params:
#   - int $1 new schema version
#
# globals:
#   - string $MYSQL_CMD
#   - string $DB_NAME
#
# returns
#   - 0 if ok
#   - 1 if fail
#
function set_schema_version (){
    sql="UPDATE database_configuration SET version = '${1}';"

    ${MYSQL_CMD} -D ${DB_NAME} -e "${sql}" 1>/dev/null

    return $?
}