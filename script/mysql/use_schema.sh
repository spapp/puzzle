#!/bin/sh
#
# puzzle - Graduated database building.
#
# GNU General Public License v3 <http://www.gnu.org/licenses/>
# Copyright (c) 2013 Sandor Papp
#
# <https://bitbucket.org/spapp/puzzle>
#

# Use a database schema.
#
# params:
#   - int $1 schema version
#
# globals:
#   - function  use_sql_schema
#   - function  use_executable_schema
#   - string    $APPLICATION_PATH
#   - string    $SCHEMA_NAME
#   - string    $MYSQL_CMD
#   - string    $LAST_ERROR
#   - string    $DB_NAME
#   - string    $DB_HOST
#   - string    $DB_PORT
#   - string    $DB_USER
#   - string    $DB_PWD
#
# returns
#   - 0 if ok
#   - 1 if fail
#
function use_schema (){
    # make tmp file for the command error output
    tmp=$(mktemp)

    # find sql file
    filename=$(find ${SCHEMA_PATH}/${SCHEMA_NAME} -name ${1}.sql)

    if [ -f "${filename}" ]
    then
        ${MYSQL_CMD} -D ${DB_NAME} < "${filename}" > ${tmp} 2>&1
        fail=$?
    else
        # find executable file
        filename=$(find ${SCHEMA_PATH}/${SCHEMA_NAME} -name ${1}.*)

        if [ -x "${filename}" ]
        then
            ${filename} ${DB_NAME} ${DB_HOST} ${DB_PORT} ${DB_USER} ${DB_PWD} > ${tmp} 2>&1
            fail=$?
        else
            LAST_ERROR="Schema file not exists or not supported (${1})."
            return 1
        fi
    fi

    if [ ${fail} -gt 0 ]
    then
        LAST_ERROR=$(cat ${tmp})
    fi

    rm -f ${tmp}

    return ${fail}
}
