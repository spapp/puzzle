#!/bin/sh
#
# puzzle - Graduated database building.
#
# GNU General Public License v3 <http://www.gnu.org/licenses/>
# Copyright (c) 2013 Sandor Papp
#
# <https://bitbucket.org/spapp/puzzle>
#

. ${APPLICATION_PATH}/script/mysql/set_schema_version.sh
. ${APPLICATION_PATH}/script/mysql/use_schema.sh

# Update the database schema.
#
# params:
#   - int $1 next version
#   - int $2 last version
#
# globals:
#   - function  set_schema_version
#   - function  use_schema
#   - string    $LAST_ERROR
#
# returns
#   - 0 if ok
#   - 1 if fail
#
function update_schema (){
    for v in $(seq $1 $2)
    do
        LAST_ERROR=
        log_begin Update ${v}. schema.
        use_schema ${v}

        if [ $? -gt 0 ]
        then
            log_end_fail

            if [ -n "${LAST_ERROR}" ]
            then
                log_error Schema update is not success!
                log_error ${LAST_ERROR}
            fi

            return 1
        fi

        LAST_ERROR=
        set_schema_version ${v}

        if [ $? -gt 0 ]
        then
            log_end_fail

            if [ -n "${LAST_ERROR}" ]
            then
                log_error Schema version update is not success!
                log_error ${LAST_ERROR}
            fi

            return 1
        fi

        log_end_ok
    done

    return 0
}



