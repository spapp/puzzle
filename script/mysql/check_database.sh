#!/bin/sh
#
# puzzle - Graduated database building.
#
# GNU General Public License v3 <http://www.gnu.org/licenses/>
# Copyright (c) 2013 Sandor Papp
#
# <https://bitbucket.org/spapp/puzzle>
#

# Checks the database is exists.
#
# params:
#   - string $1 database name
#
# globals:
#   - string $MYSQL_CMD
#
# returns
#   - 0 if ok
#   - 1 if fail
#
function check_database (){
    sql="SELECT schema_name FROM schemata WHERE schema_name='${1}'"

    db=$(${MYSQL_CMD} -D information_schema -N -e "${sql}")

    if [ -z "${db}" ]; then
        return 1
    fi

    return 0
}