#!/bin/sh
#
# puzzle - Graduated database building.
#
# GNU General Public License v3 <http://www.gnu.org/licenses/>
# Copyright (c) 2013 Sandor Papp
#
# <https://bitbucket.org/spapp/puzzle>
#

DB_HOST=${DB_HOST:-localhost}
DB_USER=${DB_USER}
DB_PWD=${DB_PWD}
DB_NAME=${DB_NAME}
DB_PORT=${DB_PORT:-3306}
# MySQL database default character set
DB_DEFAULT_CHARACTER_SET='utf8 COLLATE utf8_general_ci'

# MySQL command for the ease of use
MYSQL_CMD="mysql -h $DB_HOST -P $DB_PORT -u $DB_USER -p$DB_PWD"


# Checks the required arguments.

if [ -z "${DB_USER}" ]
then
    log_warn The database user name is required.
    has_error=1
fi

if [ -z "${DB_PWD}" ]
then
    log_warn The database password is required.
    has_error=1
fi

if [ -z "${DB_NAME}" ]
then
    log_warn The database name is required.
    has_error=1
fi
