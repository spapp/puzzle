#!/bin/sh
#
# puzzle - Graduated database building.
#
# GNU General Public License v3 <http://www.gnu.org/licenses/>
# Copyright (c) 2013 Sandor Papp
#
# <https://bitbucket.org/spapp/puzzle>
#

log_info "Update schema version: ${update_schema_version}"

if [ ${update_schema_version} -gt ${current_schema_version} ]
then
    log_begin "Make database dump."
    make_dump "${APPLICATION_PATH}/dump"

    if [ $? -gt 0 ]
    then
        log_end_fail
    else
        log_end_ok
    fi

    update_schema $(expr ${current_schema_version} + 1) ${update_schema_version}

    if [ $? -gt 0 ]
    then
        log_error Schema update failed.
        exit 1
    fi

    log_info Current schema version: $(get_schema_version)

    log_begin Schema update successful.
    log_end_ok
else
    log_warn The database current schema version is greater or equal than ${update_schema_version}.
fi