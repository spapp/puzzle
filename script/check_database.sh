#!/bin/sh
#
# puzzle - Graduated database building.
#
# GNU General Public License v3 <http://www.gnu.org/licenses/>
# Copyright (c) 2013 Sandor Papp
#
# <https://bitbucket.org/spapp/puzzle>
#

# checks the database is exists
log_info "Checks the database (${DB_NAME}) is exists."

check_database ${DB_NAME}

# if the database is not exists then create it
if [ $? -gt 0 ]
then
    log_warn "The database (${DB_NAME}) is not exists."
    log_begin Create database: ${DB_NAME}.

    create_database ${DB_NAME}

    if [ $? -gt 0 ]
    then
        log_end_fail
        exit 1
    fi

    log_end_ok
    log_info "Current schema version: -"
else
    log_begin Get current schema version.

    current_schema_version=$(get_schema_version)
    tmp_csv=$(echo ${current_schema_version} | grep -E ^[0-9]+$)

    if [ "${current_schema_version}" == "${tmp_csv}" ]
    then
        log_end_ok
        log_info "Current schema version: ${current_schema_version}"
    else
        log_end_fail
        log_error "${current_schema_version}"
        current_schema_version=-1
        log_info "Current schema version: -"
    fi
fi