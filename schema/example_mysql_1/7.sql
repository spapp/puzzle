--
-- puzzle - Graduated database building.
--
-- GNU General Public License v3 <http://www.gnu.org/licenses/>
-- Copyright (c) 2013 Sandor Papp
--
-- <https://bitbucket.org/spapp/puzzle>
--
DROP TABLE `timesheet`;
