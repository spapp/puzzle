--
-- puzzle - Graduated database building.
--
-- GNU General Public License v3 <http://www.gnu.org/licenses/>
-- Copyright (c) 2013 Sandor Papp
--
-- <https://bitbucket.org/spapp/puzzle>
--
INSERT INTO `timesheet` VALUES (
    NULL, 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'no'
), (
    NULL, 'no', 'no', 'no', 'no', 'no', 'yes', 'yes'
), (
    NULL, 'yes', 'yes', 'yes', 'no', 'no', 'no', 'no'
), (
    NULL, 'no', 'no', 'no', 'yes', 'yes', 'yes', 'no'
), (
    NULL, 'no', 'no', 'no', 'no', 'no', 'no', 'yes'
);
