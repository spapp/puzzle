--
-- puzzle - Graduated database building.
--
-- GNU General Public License v3 <http://www.gnu.org/licenses/>
-- Copyright (c) 2013 Sandor Papp
--
-- <https://bitbucket.org/spapp/puzzle>
--
DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
    `newsid` INT                                               NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `title`  VARCHAR(255)                                      NOT NULL,
    `news`   TEXT                                              NOT NULL,
    `type`   ENUM('political', 'economic', 'tabloid', 'other') NOT NULL
)
    ENGINE = MYISAM;
