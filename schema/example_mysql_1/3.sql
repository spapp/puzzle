--
-- puzzle - Graduated database building.
--
-- GNU General Public License v3 <http://www.gnu.org/licenses/>
-- Copyright (c) 2013 Sandor Papp
--
-- <https://bitbucket.org/spapp/puzzle>
--
DROP TABLE IF EXISTS `timesheet`;

CREATE TABLE `timesheet` (
    `timesheetid` INT               NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `mon`         ENUM('yes', 'no') NOT NULL,
    `tue`         ENUM('yes', 'no') NOT NULL,
    `wed`         ENUM('yes', 'no') NOT NULL,
    `thu`         ENUM('yes', 'no') NOT NULL,
    `fri`         ENUM('yes', 'no') NOT NULL,
    `sat`         ENUM('yes', 'no') NOT NULL,
    `sun`         ENUM('yes', 'no') NOT NULL
)
    ENGINE = MYISAM;
