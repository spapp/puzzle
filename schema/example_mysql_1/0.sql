--
-- puzzle - Graduated database building.
--
-- GNU General Public License v3 <http://www.gnu.org/licenses/>
-- Copyright (c) 2013 Sandor Papp
--
-- <https://bitbucket.org/spapp/puzzle>
--
CREATE TABLE database_configuration (
    version INT NOT NULL,
    PRIMARY KEY (version)
)
    ENGINE = MYISAM;

INSERT INTO database_configuration (
    version
)
    VALUES (
        0
    );
