--
-- puzzle - Graduated database building.
--
-- GNU General Public License v3 <http://www.gnu.org/licenses/>
-- Copyright (c) 2013 Sandor Papp
--
-- <https://bitbucket.org/spapp/puzzle>
--
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
    `userid`   INT          NULL AUTO_INCREMENT PRIMARY KEY,
    `username` VARCHAR(255) NULL,
    `password` VARCHAR(32)  NULL,
    `name`     VARCHAR(255) NOT NULL,
    UNIQUE (
        `username`
    )
)
    ENGINE = MYISAM;
