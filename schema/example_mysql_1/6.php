#!/usr/bin/php -q
<?php
/**
 * puzzle - Graduated database building.
 *
 * GNU General Public License v3 <http://www.gnu.org/licenses/>
 * Copyright (c) 2013 Sandor Papp
 *
 * <https://bitbucket.org/spapp/puzzle>
 */

error_reporting(0);
ini_set('display_errors', '0');

list($DUMMY, $DB_NAME, $DB_HOST, $DB_PORT, $DB_USER, $DB_PWD) = $argv;

$connection = mysqli_connect($DB_HOST, $DB_USER, $DB_PWD, $DB_NAME, $DB_PORT);

// Check connection
if (mysqli_connect_errno($connection)) {
    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
    exit(1);
}

$result = mysqli_query($connection, 'SELECT * FROM  ' . $DB_NAME . '.timesheet;');
$week = array(
    'sun',
    'mon',
    'tue',
    'wed',
    'thu',
    'fri',
    'sat'
);

while ($row = mysqli_fetch_object($result)) {
    $timesheet = new stdClass();
    $timesheet->dow = '';

    foreach ((array)$row as $day => $value) {
        if ('yes' === $value) {
            $timesheet->dow .= array_search($day, $week);
        }
    }

    $sql = 'INSERT INTO ' . $DB_NAME . '.action (actionid, data, timesheet)
                VALUES (NULL, \'\', \'' . json_encode((array)$timesheet) . '\');';

    mysqli_query($connection, $sql);
}

mysqli_free_result($result);
mysqli_close($connection);

exit(0);
