--
-- puzzle - Graduated database building.
--
-- GNU General Public License v3 <http://www.gnu.org/licenses/>
-- Copyright (c) 2013 Sandor Papp
--
-- <https://bitbucket.org/spapp/puzzle>
--
DROP TABLE IF EXISTS `action`;

CREATE TABLE `action` (
    `actionid`  INT  NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `data`      TEXT NOT NULL,
    `timesheet` TEXT NOT NULL
)
    ENGINE = MYISAM;
