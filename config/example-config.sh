#!/bin/sh
#
# puzzle - Graduated database building.
#
# GNU General Public License v3 <http://www.gnu.org/licenses/>
# Copyright (c) 2013 Sandor Papp
#
# <https://bitbucket.org/spapp/puzzle>
#

# Databbase adapter (engine).
DB_ADAPTER='mysql'
# Connect to the server on the given host.
DB_HOST='localhost'
# The user name to use when connecting to the server.
DB_USER=''
# The password to use when connecting to the server.
DB_PWD=''
# The database to use.
DB_NAME='example_db'
# The TCP/IP port number to use for the connection.
DB_PORT=

# Schema name to be used.
SCHEMA_NAME='example_mysql_1'
# path to schemas
SCHEMA_PATH=${APPLICATION_PATH}/schema



